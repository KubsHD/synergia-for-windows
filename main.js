const {app, BrowserWindow, session, Tray, Menu} = require('electron');
const path = require('path');
const url = require('url');
const autoUpdater = require("electron-updater").autoUpdater;
const log = require('electron-log');
const fs = require('fs') 
const isDev = require('electron-is-dev');




let win;
let tray = null;


if (!isDev) {
    autoUpdater.logger = log;
    autoUpdater.logger.transports.file.level = 'info';
    log.info('App starting...');
    autoUpdater.checkForUpdatesAndNotify();
}

app.once('ready', () => {
    win = new BrowserWindow({
        width:1200, 
        height:720,
        title : 'Synergia for Windows',
        icon: __dirname + '/app/icon/icon.ico',
        maximizable: false,
        webPreferences: {
            nodeIntegration: false,
            preload: './preload.js'
        },
        show: false
    
    })


    win.once('ready-to-show', () => {
        win.show()
    })
    
    tray = new Tray(__dirname + '/app/icon/icon.ico')
    const contextMenu = Menu.buildFromTemplate([
      {label: 'Wyjdź', type: 'normal', click() {app.exit()}},
    ])

    tray.setToolTip('Synergia for Windows')
    tray.setContextMenu(contextMenu)
    tray.on('click', () => {
        toggleWin()
    })

    const toggleWin = () => {
		if (win.isVisible()) {
			win.hide();
		} else {
			win.show();
		}
};
    win.on('minimize',function(event){
        event.preventDefault();
        win.hide();
    })
    
    win.on('close', function (event) {
        if(!app.isQuiting){
            event.preventDefault();
            win.hide();
        }
    })
    
    
    win.loadURL('https://synergia.librus.pl/')
    win.webContents.on('did-finish-load', () => {
        win.setTitle('Synergia For Windows')
  })
    
    win.setMenu(null)
    win.webContents.on('new-window', (event, url) => {
        event.preventDefault()
        win.setTitle('Synergia For Windows')   
    
    })
})



autoUpdater.on('update-available', () => {
  autoUpdater.downloadUpdate()
})



autoUpdater.on('update-downloaded', () => {  
    setImmediate(() => autoUpdater.quitAndInstall())
})