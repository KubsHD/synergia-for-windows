# Synergia for Windows

Prosty wrapper dla e-dziennika synergia.

## [POBIERZ](https://github.com/KubsHD/synergia-for-windows/releases)

### To-Do
- [ ] Lepszy system automatycznych aktualizacji
- [ ] Lepszy wygląd paska tytułowego
- [ ] Autostart

### Użyte oprogramowanie:
* [NodeJS](https://nodejs.org)
* [Electron](http://electronjs.org/)
* [Electron-Builder](http://electron.build/)
